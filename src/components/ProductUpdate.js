import { useState,useContext,useEffect } from "react";
import { Form, Button, Card } from "react-bootstrap";
import {Link, Navigate, useNavigate, useParams} from 'react-router-dom';

import UserContext from "../UserContext"
import Swal from 'sweetalert2';


export default function ProductUpdate() {

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  const {user} = useContext(UserContext);
	const {productId} = useParams();
  const navigate = useNavigate();


  function UpdateProduct(e) {
    e.preventDefault();

  	fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`,
  	{
  		method: "PUT",
  		headers: {
  		      	"Content-Type" : "application/json",
  		        Authorization: `Bearer ${localStorage.getItem("token")}`
  		      },
  		body: JSON.stringify({
  						name: name,
				      description: description,
				      price: price
  					}) 
  	}).then(res =>res.json()).then(data => {
			
			if (data === false) {
				Swal.fire({
					title: "Cannot update product!",
                  icon: "error"
				})

			} else {
				Swal.fire({
                  title: "Product updated successfully!",
                  icon: "success"
        })
				
      	navigate("/dashboard/allProducts");
			}
		})
	};

	// Fetching current product details to set getters:
	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/specific/${productId}`)
		      .then((res) => res.json())
		      .then((data) => {

		        setName(data.name);
		        setDescription(data.description);
		        setPrice(data.price);
		      });
	},[])

  return (
    <div className="d-flex justify-content-center mt-5">
      <Card style={{ width: "25rem" }}>
        <Card.Body>
          <Card.Title>Update Product</Card.Title>
          <Form onSubmit={(e) => UpdateProduct(e)}>

            <Form.Group controlId="formProductName">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductDescription">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                placeholder="Enter product description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
              />
            </Form.Group>

            <Button className="mt-3" variant="primary" type="submit">
              Save changes
            </Button>
            <Button className="mt-3" variant="danger" id="submitBtn" as={Link} to={"/dashboard/allProducts"}>
                        Discard Changes
                      </Button>
          </Form>
        </Card.Body>
      </Card>
    </div>
  );
}