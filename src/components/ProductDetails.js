import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';

import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import {useParams, useNavigate, Link} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function ProductDetails() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const order = (productId) => {
			fetch(`${process.env.REACT_APP_API_URL}/orders/create`, {
				method: "POST",
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productId:productId
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {
					Swal.fire({
						title: "Successfully enrolled",
						icon: "success",
						text: "You have successfully enrolled for this course."
					})

					navigate("/products")

				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}

			})
		};


	useEffect(() => {
			fetch(`${process.env.REACT_APP_API_URL}/products/specific/${productId}`)
				.then(res => res.json())
				.then(data => {
					console.log(data);

					setName(data.name);
					setDescription(data.description);
					setPrice(data.price);
				})
		}, [productId])

	return (

		<Container>
			<Row className="mt-3">
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        {
					        	(user.id !== null) ?
					        		<Button variant="primary" as={Link} to="/order">Order Now!</Button>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login">Log in to order the product</Button>
					        }
					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}
