import {Button, Nav, Navbar, Modal} from 'react-bootstrap'
import {useState, useContext} from 'react';
import {Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext';
import CartContext from '../CartContext';
import CartProduct from './CartProduct';

import Swal from 'sweetalert2';


const checkout = () => {
  Swal.fire({
      title: "Purchase Successful!",
      icon: "success",
      text: "Redirecting you back the the products page"

  }) 
};

export default function AppNavbar() {
  const {user} = useContext(UserContext);
  const cart = useContext(CartContext);

  console.log(cart);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const productsCount = cart.items.reduce((sum, product) => sum + product.quantity, 0);

  return (
    <>
      <Navbar bg="light" expand="lg">
          <Navbar.Brand as={Link} to="/">Natflix</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse className="justify-content-end" id="basic-navbar-nav">
            <Nav className  ="me-auto">
              <Nav.Link as={NavLink} to={"/"}>Home</Nav.Link>
              <Nav.Link as={NavLink} to={"/products"}>Products</Nav.Link>
            </Nav>
            <Nav>
              { (user.id !== null) ?
                 (user.isAdmin !== false) ?
                 <>
                   <Nav.Link as={NavLink} to="/dashboard">Dashboard</Nav.Link>
                   <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                 </>
                 :
              <>
              <Button onClick={handleShow}>Cart ({productsCount} Items)</Button>
              <Nav.Link as={NavLink} to={"/logout"}>Logout</Nav.Link>
              </>
              :
              <>
              <Nav.Link as={NavLink} to={"/register"}>Register</Nav.Link>
              <Nav.Link as={NavLink} to={"/login"}>Login</Nav.Link>
              </>
              }
            </Nav>
          </Navbar.Collapse>
      </Navbar>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>Shopping Cart</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            {productsCount > 0 ?
              <>
                <p>Items in your cart: </p>
                {cart.items.map((currentProduct, index) => (
                    <CartProduct key={index} id={currentProduct.id} quantity={currentProduct.quantity}></CartProduct>
                  ))}

                    <h1>Total: {cart.getTotalCost().toFixed(2)}</h1>

                    <Button id="modal" variant="success" as={Link} to={"/products"} onClick={checkout}> Purchase Items! </Button>
              </>
            :
            <h1>There are no items in your cart!</h1>
          }
        </Modal.Body>
      </Modal>
    </>
  );  
}