
import {useContext} from 'react';
import { Button, Row, Col, Card } from 'react-bootstrap';
import {Link, Navigate} from 'react-router-dom';

import UserContext from "../UserContext"

import Swal from 'sweetalert2';

export default function ProductCard({product}) {

const {user} = useContext(UserContext);
// const {product} = useContext(UserContext);
const { name, description, price, _id } = product;

    function handleDeactivate(e) {

        fetch(`${process.env.REACT_APP_API_URL}/products/archive/${_id}`,
            {
              method: "PATCH",
              headers:
                    {
                      "Content-Type" : "application/json",
                      Authorization: `Bearer ${localStorage.getItem("token")}`
                    } 
            }).then(res =>res.json()).then(data =>
            {

              if (data === false) {
                Swal.fire({
                    title: "Error",
                    icon: "error"
                })
              } else {
                Swal.fire({
                    title: "Product deactivated!",
                    icon: "success"
                })

                Navigate("/dashboard/allProducts");
              }
            })
      }

      // admin only function for restoring archived products
      function handleRestore(e) {

        fetch(`${process.env.REACT_APP_API_URL}/products/restore/${_id}`,
          {
            method: "PATCH",
            headers:
                  {
                    "Content-Type" : "application/json",
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                  } 
          }).then(res =>res.json()).then(data => {
            // console.log(data)
            if (data.error) {
            Swal.fire({
                title: "Error",
                icon: "error"           
              })
            } else {
            Swal.fire({
                title: "Product reactivated!",
                icon: "success"
            })        
              Navigate("/dashboard/archives");
            }
          })
      }


return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>PHP {price}.00 per gallon</Card.Text>

                        {(user.isAdmin) ?
                            (product.isActive) ?
                            <>
                            <Button className="bg-primary" as={Link} to={`/dashboard/update/${_id}`}>Update</Button>
                            <Button className="bg-danger" onClick={handleDeactivate}>Deactivate</Button>
                            </>
                            :
                            <Button className="bg-success" onClick={handleRestore}>Reactivate</Button>
                        :
                        <Button className="bg-primary" as={Link} to={`/products/${_id}`} >Details</Button>
                        }

                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}
