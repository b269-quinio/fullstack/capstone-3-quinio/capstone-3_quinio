import {useState, useEffect} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {Container} from 'react-bootstrap';

import {UserProvider} from './UserContext';
import {CartProvider} from './CartContext';

import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Products from './pages/Products';
import Order from './pages/Order';
import ProductDetails from './components/ProductDetails';


import Dashboard from './pages/Dashboard';
import CreateProduct from './components/CreateProduct';
import AllProducts from './components/AllProducts';
import ProductUpdate from './components/ProductUpdate';
import ArchiveProducts from './components/Archives';

import './App.css';

function App() {

  // const [user, setUser] = useState({email: localStorage.getItem('email')});

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      // User is logged in
      if(typeof data._id !== 'undefined') {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      // User is logged out
      else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })
  }, []);

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>

        <Router>
              <CartProvider>
          <Container>
          <AppNavbar/>
          <Routes>
            < Route path="/dashboard" element={<Dashboard/>}/>
            < Route path="/" element={<Home/>}/>
            < Route path="/register" element={<Register/>}/>
            < Route path="/login" element={<Login/>}/>
            < Route path="/logout" element={<Logout/>}/>
            < Route path="*" element={<Error/>}/>
            < Route path="/order" element={<Order/>}/>
            < Route path="/products" element={<Products/>}/>
            < Route path="/products/:productId" element={<ProductDetails/>}/>
            < Route path="/dashboard/create" element={<CreateProduct/>}/>
            < Route path="/dashboard/allProducts" element={<AllProducts/>}/>
            < Route path="/dashboard/update/:productId" element={<ProductUpdate/>}/>
            < Route path="/dashboard/archives" element={<ArchiveProducts/>}/>
          </Routes>
          </Container>
                </CartProvider>
        </Router>

      </UserProvider>

    </>
  );
}

export default App;
