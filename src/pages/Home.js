import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {
	return (
		<>
		< Banner
		title="Natflix"
		subtitle= "We supply your daily architectural needs!"
		destination = "/products"
		button= "Order now!"
		/>
		<Highlights/>
		</>
	)
}
