import { useContext,useState, useEffect } from 'react';
import { Card, Container } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';

import UserContext from '../UserContext';

export default function Dashboard() {

const {user} = useContext(UserContext);
const navigate = useNavigate();
const [dashboard, setDashboard] = useState();

  useEffect(()=> {
    if(user.isAdmin) {
         setDashboard(
        <>
        <Container className="overflow-auto">
          <Card className="p-5 justify-content-center dashboardCard flex-column mb-5" as={Link} to="/dashboard/create">
            <div className="d-flex justify-content-center align-items-center">
              <div className="p-0">
                {/*<Card.Img variant="top" src={AddProduct} className="card-img" style={{ width: '200px', height: '200px' }} />*/}
              </div>
              <div className="p-4">
                <Card.Title className="card-title">Create New Product</Card.Title>
              </div>
            </div>
          </Card>

          <Card className="p-5 justify-content-center dashboardCard flex-column mb-5" as={Link} to="/dashboard/allProducts">
            <div className="d-flex justify-content-center align-items-center">
              <div className="p-0">
                {/*<Card.Img variant="top" src={AllProducts} className="card-img" style={{ width: '200px', height: '200px' }} />*/}
              </div>
              <div className="p-4">
                <Card.Title className="card-title">Products Catalogue & Information Update</Card.Title>
              </div>
            </div>
          </Card>

          <Card className="p-5 justify-content-center dashboardCard flex-column mb-5" as={Link} to="/dashboard/archives">
            <div className="d-flex justify-content-center align-items-center">
              <div className="p-0">
                {/*<Card.Img variant="top" src={Archives} className="card-img" style={{ width: '200px', height: '200px' }} />*/}
              </div>
              <div className="p-4">
                <Card.Title className="card-title">Archives</Card.Title>
              </div>
            </div>
          </Card>
        </Container>
        </>

        );
    } else {

       navigate("/");

    }
     
  },[user])

    	return(
    		<>
    		{dashboard}
    		</>)
};

